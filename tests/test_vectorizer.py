from ernie.vectorizer import BertVectorizer

BERT_PATH = 'https://tfhub.dev/google/bert_multi_cased_L-12_H-768_A-12/1'
MAX_SEQUENCE_LENGTH = 512

vectorizer = BertVectorizer.from_bert_path(BERT_PATH, MAX_SEQUENCE_LENGTH)


def test_vectorizer_transform_sentence():
    x = vectorizer.transform_sentence('This is a sentence')
    assert len(x) == 3
    assert x[0].shape == (MAX_SEQUENCE_LENGTH,)
    assert x[1].shape == (MAX_SEQUENCE_LENGTH,)
    assert x[2].shape == (MAX_SEQUENCE_LENGTH,)


def test_vectorizer_transform_multiple_sentences():
    x = vectorizer.transform(['This is a sentence', 'And this is another'])
    assert len(x) == 3
    assert x[0].shape == (2, MAX_SEQUENCE_LENGTH)
    assert x[1].shape == (2, MAX_SEQUENCE_LENGTH)
    assert x[2].shape == (2, MAX_SEQUENCE_LENGTH)
