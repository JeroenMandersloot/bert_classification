import os
import random
from itertools import islice
from typing import Iterator, Any, Generator, Tuple, List

import numpy as np
import tensorflow as tf

from bert_classification.data.dataset import Dataset
from bert_classification.util import listpath
from bert_classification.vectorizer import BertVectorizer


class ImdbDataset(Dataset):
    def __init__(self, root: str, vectorizer: BertVectorizer):
        self.root = root
        self.vectorizer = vectorizer
        self.data = self._parse_data()

    def _parse_data(self) -> List[Tuple[str, int]]:
        return [(f, 1) for f in listpath(os.path.join(self.root, 'pos'))] + \
               [(f, 0) for f in listpath(os.path.join(self.root, 'neg'))]

    @staticmethod
    def get_text(x: Tuple[str, int]) -> str:
        path, _ = x
        with open(path, 'r') as f:
            return f.read()

    @staticmethod
    def get_label(x: Tuple[str, int]) -> int:
        return x[1]

    def batches(self, batch_size: int) -> Generator[
        Tuple[Tuple[tf.Tensor, tf.Tensor, tf.Tensor], tf.Tensor], None, None]:
        data = iter(random.sample(self.data, len(self.data)))
        batch = list(islice(data, batch_size))
        while len(batch) == batch_size:
            texts = [self.get_text(x) for x in batch]
            labels = [self.get_label(x) for x in batch]
            x = list(map(np.array, self.vectorizer.transform(texts)))
            y = np.array(labels)
            yield x, y
            batch = list(islice(data, batch_size))

    def __len__(self) -> int:
        return len(self.data)

    def __iter__(self) -> Iterator[Any]:
        return iter(self.data)
