import abc
from typing import Any, Iterator


class Dataset:
    @abc.abstractmethod
    def batches(self, batch_size: int):
        """
        A generator that yields batches of size `batch_size`. Each batch is a
        tuple of data and label arrays that can be fed directly to a model's
        `fit()` method.

        :param batch_size: int
        :return:
        """
        raise NotImplementedError

    @abc.abstractmethod
    def __len__(self) -> int:
        """
        Returns the size of the dataset.

        :return: int
        """
        raise NotImplementedError

    @abc.abstractmethod
    def __iter__(self) -> Iterator[Any]:
        """
        Returns an iterator over the instances in the dataset.

        :return: Iterator[Any]
        """
        raise NotImplementedError
