import os
from typing import List

import tensorflow as tf
import tensorflow.python.keras.backend as K


def listpath(folder: str) -> List[str]:
    """
    Returns a list of files in a directory, similar to `os.listdir`, except
    the whole path to the file is returned instead of just the filename.

    :param folder: str
    :return: List[str]
    """
    return [os.path.join(folder, f) for f in os.listdir(folder)]


def initialize_session(session=None):
    """
    A helper function to perform some standard TensorFlow initialization.

    :param session:
    """
    if not session:
        session = tf.Session()
    session.run(tf.local_variables_initializer())
    session.run(tf.global_variables_initializer())
    session.run(tf.tables_initializer())
    K.set_session(session)


def fix_tensorflow_rtx():
    """
    A fix for RTX 2070 card, without this the code doesn't run.
    """
    gpu_devices = tf.config.experimental.list_physical_devices('GPU')
    for device in gpu_devices:
        tf.config.experimental.set_memory_growth(device, True)
