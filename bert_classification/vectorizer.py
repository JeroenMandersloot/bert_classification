from typing import List

import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
from bert.tokenization import FullTokenizer


class BertVectorizer:
    def __init__(self, tokenizer, max_sequence_length: int):
        self.tokenizer = tokenizer
        self.max_sequence_length = max_sequence_length

    @classmethod
    def from_bert_path(cls, bert_path: str, max_sequence_length: int):
        bert_module = hub.Module(bert_path)
        tokenization_info = bert_module(
            signature="tokenization_info",
            as_dict=True
        )

        with tf.Session() as sess:
            vocab_file, do_lower_case = sess.run(
                [tokenization_info["vocab_file"],
                 tokenization_info["do_lower_case"]]
            )

        tokenizer = FullTokenizer(
            vocab_file=vocab_file,
            do_lower_case=do_lower_case
        )

        return cls(tokenizer, max_sequence_length)

    def transform(self, sentences: List[str]):
        return list(map(lambda t: np.stack(t, axis=0),
                        zip(*[self.transform_sentence(s) for s in sentences])))

    def transform_sentence(self, sentence: str):
        tokens = self.tokenizer.tokenize(sentence)
        tokens = ['[CLS]'] + tokens[:self.max_sequence_length - 2] + ['[SEP]']

        # Create the inputs without padding.
        input_ids = self.tokenizer.convert_tokens_to_ids(tokens)
        input_mask = [1] * len(tokens)
        segment_ids = [0] * len(tokens)

        # Pad all inputs to `self.max_sequence_length`.
        pad_length = self.max_sequence_length - len(tokens)
        input_ids += [0] * pad_length
        input_mask += [0] * pad_length
        segment_ids += [0] * pad_length

        return np.array(input_ids), np.array(input_mask), np.array(segment_ids)
