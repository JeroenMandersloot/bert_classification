import re
from typing import Tuple, List

import tensorflow as tf
import tensorflow.python.keras.backend as K
import tensorflow_hub as hub


class BertLayer(tf.keras.layers.Layer):
    def __init__(self,
                 bert_path: str,
                 num_fine_tune_layers: int = 0,
                 **kwargs):
        self.bert_path = bert_path
        self.num_fine_tune_layers = num_fine_tune_layers
        self.trainable = bool(self.num_fine_tune_layers)
        self.bert = None
        self.num_layers, self.output_size, _ = self.get_config()
        super(BertLayer, self).__init__(**kwargs)

    def get_config(self) -> Tuple[int, int, int]:
        """
        Returns several model configuration settings that can be deduced from
        the model's hub url. These are, in order, the number of encoder layers
        L, the dimensionality of the final output layer H, and the number of
        attention heads A.

        :return: Tuple[int, int, int]
        """
        num_layers = int(re.findall(r'L-(\d+)', self.bert_path)[0])
        output_size = int(re.findall(r'H-(\d+)', self.bert_path)[0])
        num_attention_heads = int(re.findall(r'A-(\d+)', self.bert_path)[0])
        return num_layers, output_size, num_attention_heads

    def get_trainable_weights(self) -> List:
        """
        Returns a list of TensorFlow variables that are trainable within the
        BERT architecture.

        :return: List
        """
        if not self.trainable:
            return []
        trainable_layers = range(
            self.num_layers - self.num_fine_tune_layers, self.num_layers)
        pattern = r'layer_({})'.format('|'.join(map(str, trainable_layers)))
        weights = []
        for var in self.bert.variables:
            if re.search(pattern, var.name):
                weights.append(var)
        return weights

    def build(self, input_shape):
        self.bert = hub.Module(self.bert_path, trainable=False)
        trainable_weights = self.get_trainable_weights()
        for var in self.bert.variables:
            if var in trainable_weights:
                self._trainable_weights.append(var)
            else:
                self._non_trainable_weights.append(var)
        super(BertLayer, self).build(input_shape)

    def call(self, inputs: Tuple[tf.Tensor, tf.Tensor, tf.Tensor], **kwargs):
        inputs = [K.cast(x, dtype="int32") for x in inputs]
        input_ids, input_mask, segment_ids = inputs
        bert_inputs = dict(
            input_ids=input_ids,
            input_mask=input_mask,
            segment_ids=segment_ids
        )

        return self.bert(
            inputs=bert_inputs,
            signature="tokens",
            as_dict=True
        )['pooled_output']

    def compute_output_shape(self, input_shape):
        return input_shape[0], self.output_size
