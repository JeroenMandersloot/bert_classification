from tensorflow.python.keras import Input, Model
from tensorflow.python.keras.layers import Dense

from bert_classification.model.bert_layer import BertLayer
from bert_classification.util import initialize_session


def build_model(bert_path: str,
                max_sequence_length: int,
                num_fine_tune_layers: int) -> Model:
    input_ids = Input(shape=(max_sequence_length,))
    input_mask = Input(shape=(max_sequence_length,))
    segment_ids = Input(shape=(max_sequence_length,))
    inputs = [input_ids, input_mask, segment_ids]

    output = BertLayer(bert_path, num_fine_tune_layers)(inputs)
    output = Dense(1, activation='sigmoid')(output)

    model = Model(inputs, output)
    initialize_session()
    return model
