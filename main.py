import os
import tarfile

import confidence
import requests
from tensorflow.python.keras.optimizers import Adam

from bert_classification.data.imdb import ImdbDataset
from bert_classification.model.model import build_model
from bert_classification.util import fix_tensorflow_rtx
from bert_classification.vectorizer import BertVectorizer

fix_tensorflow_rtx()


def download_imdb(output_dir, verbose=True):
    # Check if the IMDb dataset has already been downloaded.
    archive_file = os.path.join(output_dir, 'imdb.tar.gz')
    if os.path.exists(archive_file):
        return

    if verbose:
        print('Downloading IMDb dataset...')

    # Download the archive file from the internet.
    url = 'https://ai.stanford.edu/~amaas/data/sentiment/aclImdb_v1.tar.gz'
    r = requests.get(url, allow_redirects=True)
    with open(archive_file, 'wb') as f:
        f.write(r.content)

    if verbose:
        print('Extracting IMDb dataset...')

    # Extract the data.
    with tarfile.open(archive_file, "r:gz") as tar:
        tar.extractall(path=output_dir)


def main(config):
    # Read config settings.
    bert_path = config.bert.model.hub_base_url + config.bert.model.hub_module
    max_sequence_length = config.bert.model.max_sequence_length
    data_root = config.bert.data.root
    imdb_train_root = config.bert.data.imdb.train.root
    num_fine_tune_layers = config.bert.training.num_fine_tune_layers
    learning_rate = config.bert.training.learning_rate
    batch_size = config.bert.training.batch_size
    num_epochs = config.bert.training.num_epochs
    model_path = config.bert.model.path

    # Create the vectorizer used to pre-process the input texts for BERT.
    vectorizer = BertVectorizer.from_bert_path(bert_path, max_sequence_length)

    # Make sure the IMDb dataset is downloaded.
    download_imdb(data_root)

    # Build and compile the model.
    model = build_model(bert_path, max_sequence_length, num_fine_tune_layers)
    optimizer = Adam(lr=learning_rate)
    model.compile(
        optimizer=optimizer,
        loss='binary_crossentropy',
        metrics=['accuracy']
    )

    # Load the IMDb dataset.
    dataset = ImdbDataset(imdb_train_root, vectorizer)
    batch_size = batch_size
    steps_per_epoch = len(dataset) // batch_size

    # Create an endless generator to use with `fit_generator()`.
    def generator():
        while True:
            yield from dataset.batches(batch_size)

    # Load the weights from a previously saved model if they exist.
    try:
        model.load_weights(model_path)
    except FileNotFoundError:
        print('No previous model found, starting from scratch...')

    # Start the training loop.
    try:
        model.fit_generator(
            generator(),
            steps_per_epoch=steps_per_epoch,
            epochs=num_epochs,
            verbose=1
        )
    except KeyboardInterrupt:
        pass

    # After finishing or interrupting training, save the model.
    model.save(model_path)


if __name__ == '__main__':
    main(confidence.load_name('bert', missing=confidence.Missing.error))
