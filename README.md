This a proof of concept repository for training a BERT sentence classification
model with Keras in TensorFlow 1.14 outside of eager mode.

To train a model, first install the requirements with the following command: 
```bash
pip install -r requirements.txt
```

Afterwards, you should be able to start training immediately by running the 
`main.py` script. If this is the first time running this script, the IMDb 
dataset will be automatically downloaded from the internet. 

Afterwards, a training loop will started for whatever number of epochs you 
specified in the config file (`bert.yaml`). You can interrupt the training
process at any time. The model will be saved before terminating the script
completely.